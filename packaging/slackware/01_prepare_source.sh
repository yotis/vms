#/bin/sh

VERSION=${VERSION:-0.2.1}

last_commit=$( git log -n1 | grep commit )
COMMIT=$( echo $last_commit | cut -c 8- - | cut -c -12 - )
echo preparing $COMMIT

if [ ! -f $COMMIT.tar.gz ]; then
    wget https://bitbucket.org/yotis/vms/get/$COMMIT.tar.gz
fi

tar xvf $COMMIT.tar.gz
mv yotis-vms-$COMMIT vms-$VERSION
tar --owner 0 --group 0 -cvf vms-$VERSION.tar.gz  vms-$VERSION
rm -r vms-$VERSION
new_md5=$( md5sum vms-$VERSION.tar.gz | cut -c -32 - )
echo $new_md5
sed -i -e "s/MD5SUM=.*/MD5SUM=\"${new_md5}\"/g" vms/vms.info
mv vms-$VERSION.tar.gz vms/
