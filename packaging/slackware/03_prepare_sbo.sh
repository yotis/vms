#!/bin/sh

mv vms/vms-*.tar.gz .

if [ -f vms.tar.gz ]; then
    rm vms.tar.gz
fi

tar --owner 0 --group 0 -cvf vms.tar.gz vms/
