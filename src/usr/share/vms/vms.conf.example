#!/bin/bash

##
## This is an example vms.conf that exists in ~/.vms/vms.conf
## This file lists all the available options to the parser
## Use it as a reference or a starting point when creating a vm
## Copy, paste, edit and uncomment accordingly
## All lines starting with '##' should be deleted
##
## Options that end with numbers can be used for multiple devices
## eg. disk0, disk1, nic0, nic1, ...
##
## This is needed for declaring the vm named avm
declare -A avm
## The next three are mandatory for every vm
## 'vms create avm' can be used to create these
avm[name]=avm
avm[uuid]='ffffffff-ffff-ffff-ffff-ffffffffffff'
avm[arch]=x86_64

## The rest are all the optional possible options

# avm[disk0]=/path/to/disk.qcow2
# avm[vde0]='net1,sock=/path/to/vde-socket'
# avm[br0]='net0'
# avm[br1]='net1,br=br1'
# avm[nic0]='virtio-net-pci,mac=FF:FF:FF:FF:FF:FF,netdev=net0'
# avm[nic1]='virtio-net-pci,mac=FF:FF:FF:FF:FF:FF,netdev=net1'
# avm[cpu]='host'
# avm[mem]=1024
# avm[smp]=2
# avm[rtc]='base=utc'
# avm[bootcd]=/path/to/boot.iso
# avm[vnc]=5
# avm[kvm]=yes
# avm[log]=yes
# avm[serial]=yes
# avm[monitor]=yes

## Any extra lines can be passed as raw parameters to the qemu command line 
## as shown below
# avm[extra0]='-nodefaults'
# avm[extra1]='-device vfio-pci,host=06:00.0,romfile=/path/to/device-bios.rom'
# avm[extra2]='-usbdevice tablet -k en-us'

## Set deamon key to yes. This way the machine can be used during startup.
# avm[daemon]=yes

## Set secure key to yes. avm is intented to be used by root via eg.
## /etc/rc.d/rc.vms start
## Also, secure implies 'avm[deamon]=yes'
# avm[secure]=yes
