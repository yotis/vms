#!/bin/bash

DIR=$( dirname "${BASH_SOURCE[0]}" )
. /usr/bin/vms-parse

VM_NAME=$(basename "$0" | cut -d'.' -f1)
unset VM; declare -A VM
setup_VM $VM_NAME || exit 1

# Basic error checking
if [ "$(declare | grep $VM_NAME)" = "" ]; then
    echo "$VM_NAME configuration missing from $HOME/.vms/vms.conf"
    exit 1
fi

# Call qemu
qemu-system-${VM[arch]} \
    -name ${VM[name]} -uuid ${VM[uuid]} -pidfile ${VM[pid]} \
    $RTC $KVM $CPU $SMP $MEM $KVM $DISKS $CDROM $BOOTCD $VNC \
    $BRS $VDES $NICS $SERIAL $CONSOLE $EXTRAS $SECURE $LOG $DAEMON

result=$?
if [ $result -ne 0 ]; then
    echo "qemu error $result"
    exit 1
fi
