% VMS(1) vms 0.2.1 | vms command manual
% Nikos Giotis <nikos.giotis@gmail.com>
% 28 August 2018

NAME
====

**vms** - a command for managing and creating virtual machines

SYNOPSIS
========

**vms** [_option_]

DESCRIPTION
===========

The **vms** command is used for creating and managing qemu virtual machines.
The configuration for all the user's virtual machines is stored under the
**~/.vms** directory. There is a per user configuration file 
**~/.vms/vms.conf** that stores the settings for all the user's virtual 
machines. See **vms.conf(5)** for more information about virtual machine
configuration.

OPTIONS
=======

list

:   List the current user's virtual machines

create _vm_

:   Create a virtual machine named _vm_

info _vm_

:   Display the configuration of _vm_ as defined in ~/.vms/vms.conf

conf _vm_ _key_ _value_

:   Edit the configuration of _vm_ in ~/.vms/vms.conf. Add a new
line or modify an existing line to this _vm[key]=value_. Use this
syntax \`vms conf vm key '"value"'\` to create this line
_vm[key]="value"_. The special _value_ -- can be used to delete
the _key_ line.

status _vm_

:   Display the running status of _vm_

start _vm_

:   Start the virtual machine named _vm_

stop _vm_ [_timeout_]

:   Stop the _vm_ virtual machine by sending it an ACPI system shutdown event

restart _vm_ [_timeout_]

:   Restart _vm_ by calling **stop** and **start**

kill _vm_

:   Forcibly kill the qemu process for _vm_

vnc _vm_ [**-v**]

:   Connect to the VNC console, if _vm_ is configured with one. If 
**-v** is specified, the stdout of vncviewer is shown

serial _vm_

:   Connect to the serial console, if _vm_ is configured with one

monitor _vm_

:   Connect to the qemu monitor console, if _vm_ is configured with one

log _vm_

:   Show the _vm_'s log, if _vm_ is configured with one
    
EXAMPLES
========

As a normal user, create a virtual machine named _bvm_

    vms create bvm

Edit **~/.vms/vms.conf**. See the example in **vms.conf(5)**.
Additionaly, the **conf** option can be used like this

    vms conf bvm mem 2048
    vms conf bvm smp 2
    vms conf bvm disk0 ~/.vms/bvm/disk0.img
    vms conf bvm bootcd '"$VAR_IN_VMS_CONF/install-dvd.iso"'

Any key can be deleted like this

    vms conf bvm bootcd --

Show information about _bvm_ and it's running status

    vms info bvm
    vms status bvm

To start, stop or kill _bvm_

    vms start bvm
    vms stop bvm
    vms kill bvm

To connect to the qemu monitor or VNC server, if these are
configured

    vms monitor bvm
    vms vnc bvm

DEDICATION
==========

    Gb Z.
    gb lbh, zl ybir

SEE ALSO
========

vms.conf(5), rc.vms(8), vms-tutorial(7)
