% VMS-TUTORIAL(7) vms 0.2.1 | vms tutorial
% Nikos Giotis <nikos.giotis@gmail.com>
% 28 August 2018

Create the virtual machine named vm1

    vms create vm1
    vms list

You can go ahead and test your vm

    vms info vm1
    vms start vm1

if you want to kill the qemu process, use 

    vms kill vm1

Use 2 cores, change the cpu type and enable kvm
    
    vms conf vm1 smp 2
    vms conf vm1 cpu host
    vms conf vm1 kvm yes

To delete a key use the following syntax

    vms conf vm1 cpu --
    vms conf vm1 kvm --

To use 2 GB of memory

    vms conf vm1 mem 2048

Now, let's create a disk image file
    
    qemu-img create -f qcow2 ~/.vms/vm1/disk.img 20G

And attach it to vm1

    vms conf vm1 disk0 '~/.vms/vm1/disk.img'

If you want, set vm1 to run as a daemon and add a VNC console to it

    vms conf vm1 daemon yes
    vms conf vm1 vnc 7

You can always edit the **~/.vms/vms.conf** file with your favorite
an editor.
    
Now, start vm1

    vms start vm1

If you started with vnc, connect to it like this

    vms vnc vm1

If you try to stop vm1 now with

    vms stop vm1

You will notice it will not stop, it needs a proper os installed handle
the ACPI virtual button press and vm1[monitor]=yes You can always kill
vm1 with

    vms kill vm1

Now, attach a cdrom and insert an .iso disk image to boot from

    vms conf vm1 bootcd '~/.vms/os/slackware64-14.2-install-dvd.iso'

Start the vm and install the operating system

    vms start vm1

Again, if you started with vnc, connect to it with

    vms vnc vm1

Install the os :)

# SEE ALSO

vms(1), vms.conf(5), rc.vms(8), rc.vms.conf(5)