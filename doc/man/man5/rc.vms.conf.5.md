% RC.VMS.CONF(5) vms 0.2.1 | rc.vms.conf configuration file
% Nikos Giotis <nikos.giotis@gmail.com>
% 28 August 2018

NAME
====

**rc.vms.conf** - **rc.vms** configuration file

DESCRIPTION
===========

The **rc.vms.conf** defines a set of virtual machines. Almost all the 
options passed to the **rc.vms** are operations to be done on this set.
Each machine is specified with one line like this

     username virtual_machine

Lines starting with '#' are comments

EXAMPLE
=======

Assume user _sam_ has the virtual machines named _vm01_ and _vm02_ and
the administrator wants to start them during the host's boot by calling
\`rc.vms start\`. An example **rc.vms.conf** to do this would be like 
this.

    # rc.vms.conf
    sam vm01
    sam vm02

SEE ALSO
========
rc.vms(8)
