% VMS.CONF(5) vms 0.2.1 | ~/.vms/vms.conf file format
% Nikos Giotis <nikos.giotis@gmail.com>
% 27 August 2018

NAME
====

**vms.conf** - **vms** per user configuration file format

SYNOPSIS
========

The **vms.conf** is a per user configuration file that is located at 
**~/.vms/vms.conf**. It is used by the **vms** command. It stores 
**bash** associative arrays, one for each virtual machine.

DESCRIPTION
===========

An example **vms.conf** is located at **/usr/share/vms/vms.conf.example**.
A typical **vms.conf** can be like this

    #!/bin/bash

    VMSTORE=/path/to/directory

    declare -A avm
    avm[name]=avm
    avm[uuid]='xxxxxxxx-ffff-ffff-ffff-ffffffffffff'
    avm[arch]=x86_64
    avm[mem]=2048
    avm[smp]=2
    avm[disk0]="$VMSTORE/avm-system.img"
    avm[bootcd]="$VMSTORE/install-dvd.iso"

    # declare -A bvm
    # bvm[name]=bvm
    # bvm[...

A set of predefined keys are supported by a very simple parser
(**vms-parse**). The parser uses the value for the key in the array
to compose an option for qemu. For example, for the following line

    bvm[br0]='net0,br=br1'
    
the parser will compose this qemu option

    -netdev bridge,id=net0,br=br1

The keys **name**, **uuid** and **arch** are required
for each virtual machine. Some keys can be numbered, like **disk0**,
**disk1**, **nic0**, **nic1**, etc. These are referred as **disk#** and
**nic#**. Some keys are boolean, these can be set to _yes_ or _no_.
The configuration can also be edited with **vms conf**.

REQUIRED KEYS
=============

name

:   The name of the virtual machine

uuid

:   A uuid for the virtual machine, see uuidgen(1)

arch

:   The virtual machine architecture, eg. x86_64, arm

SUPPORTED KEYS
==============

disk#

:   A path to a disk to attach to the virtual machine

vde#

:   A VDE network device

br#

:   A bridge network device

nic#

:   A network interface device

extra#

:   Raw qemu command line options

cpu

:   The qemu cpu type

mem

:   The amount of virtual machine memory

smp

:   SMP configuration for the virtual machine

rtc

:   The virtual machine's RTC configuration

bootcd

:   A path to an disk.iso

vnc

:   The VNC number to use for qemu's VNC console

kvm

:   Enable KVM, boolean

log

:   Enable logging, boolean

serial

:   Enable serial console, boolean

monitor

:   Enable monitor console, boolean

daemon

:   Run qemu as a daemon, boolean

secure
:   Run qemu as a daemon, as user nobody inside an empty chroot, boolean
    
EXAMPLE
=======

Expanding upon the avm example from above, declare the _bvm_ virtual 
machine. Let's attach the _bvm[disk0]_ to _virtio_.

    declare -A bvm
    bvm[name]=bvm
    bvm[uuid]='xxxxxxxx-ffff-ffff-ffff-ffffffffffff'
    bvm[arch]=x86_64
    bvm[mem]=2048
    bvm[smp]=2
    bvm[bootcd]="$VMSTORE/install-dvd.iso"
    bvm[disk0]="$VMSTORE/bvm-system.img,if=virtio,index=0"

Enable KVM and configure the guest cpu. Notice that for some options
like **cpu**, anything can be passed to qemu, like the _-M q35_. See
**vms-parse** for more details. Alternatively, the same can be done by
using the **extra#** keys.

    bvm[kvm]=yes
    bvm[cpu]='host -M q35'

    ## Or comment the above and use the next line to emulate a specific
    ## Intel model with kvm disabled on the guest, overriding the cpu
    ## vendor for the guest, even when qemu is running on an AMD host.
    #bvm[cpu]='Penryn,kvm=off,vendor=GenuineIntel'

Connect to two network bridges on the host. One with a _virtio-net-pci_
interface and the other with an _e1000_ on the guest.

    bvm[br0]=net0
    bvm[nic0]='virtio-net-pci,mac=52:54:00:XX:XX:XX,netdev=net0'
    bvm[br1]='net1,br=br1'
    bvm[nic1]='e1000,mac=52:54:00:XX:XX:XX,netdev=net1'

Connect to two VDE switches running on the host.

    bvm[vde0]=net2
    bvm[nic2]='virtio-net-pci,mac=52:54:00:XX:XX:XX,netdev=net2'
    bvm[vde1]=net3,sock=/path/to/vde.sock
    bvm[nic3]='e1000,mac=52:54:00:XX:XX:XX,netdev=net3'

Start VNC server and run as a daemon

    bvm[vnc]=7
    bvm[daemon]=yes

Now, one can start _bvm_ and display it's status by using

    $ vms start bvm
    $ vms status bvm

Assuming the user that owns _bvm_ is called _joe_, _bvm_ can be
configured to start during the host's boot sequence like this

    # echo 'joe bvm' >> /etc/rc.d/rc.vms.conf

SEE ALSO
========

**vms(1)**