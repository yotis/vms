% RC.VMS(8) vms 0.2.1 | rc.vms init script manual
% Nikos Giotis <nikos.giotis@gmail.com>
% 28 August 2018

NAME
====

**rc.vms** - an init script to use with **vms**

SYNOPSIS
========

**rc.vms** [_option_]

DESCRIPTION
===========

The **rc.vms** script is to be used by the administrator. It can be
used for controlling a set of virtual machines. This set is specified
in **rc.vms.conf** configuration file. It can also be used to start and
stop this set during the host's boot and shutdown sequence. The **cmd**
option can be used by the administrator to control any virtual machine
on the host.

OPTIONS
=======

status

:   display the running status of all the virtual machines in the set

start

:   start the virtual machine set, can be used during host's boot
sequence. The machines in the set are started in the order that they
appear in **rc.vms.conf**

stop

:   Stop the virtual machine set, can be used during host's shutdown
sequence. The order the machines are stopped is the reverse of the
order that they appear in **rc.vms.conf**

restart

:   Restart the virtual machine set

cmd _username_ _vms-option_ [_vm_]

:   Call \`**vms** _vms-option_ _vm_\` to control the virtual machine
named _vm_ that belongs to the user _username_. It can be used on any
virtual machine on the host, even if the virtual machine is not 
included in the set defined in **rc.vms.conf**.

EXAMPLES
========

Enable _vm01_ to run on host boot

    echo 'sam vm01' >> /etc/rc.d/rc.vms.conf

To display the status of the set and start it 

    rc.vms status
    rc.vms start

To stop the set, use

    rc.vms stop

Assume user _sam_ has a virtual machine named _vm01_. The administrator
can display the information about _vm01_ like this

    rc.vms cmd sam list
    rc.vms cmd sam info vm01

This results in calling \`vms info vm01\` to the _vm01_ that _sam_ has
defined in his **~/.vms/vms.conf**. Likewise, all **vms** options can
be used, eg

    rc.vms cmd sam status vm01
    rc.cms cmd sam start vm01

SEE ALSO
========
**vms(1)**, **rc.vms.conf(5)**
