#!/bin/bash

from_version=$1
to_version=$2

if [ "$from_version" != "" -a "$to_version" == "" ]; then
    echo "Both versions are needed"
    exit 1
fi

if [ "$from_version" != "" -a "$to_version" != "" ]; then
    for md in $( ls man/man?/*.md ); do
        sed -i -e "s/$from_version/$to_version/g" $md
    done
fi

#for f in $( find . * -type f ); do grep '0.1.2' $f; done  |less

cd .. 
pandoc README.md -s -t html > README.html
cd doc

for md in $( ls man/man?/*.md ); do
    pandoc $md -s -t man > ${md::-3}
done
